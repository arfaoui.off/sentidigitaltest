import { useRef, useState, useEffect } from 'react';
import {
	faCheck,
	faTimes,
	faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Login from './Login';
import axios from './api/axios';

const USER_REGEX = /^[A-z][A-z0-9-_]{3,23}$/;
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PHONE_REGEX = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]{7,23}$/;
const PROFESSION_REGEX = /^\d+$/;
const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/;
const REGISTER_URL = '/signup/';

const Register = () => {
	const userRef = useRef();
	const errRef = useRef();

	const [username, setUser] = useState('');
	const [validName, setValidName] = useState(false);
	const [userFocus, setUserFocus] = useState(false);

	const [first_name, setUserFirstName] = useState('');
	const [validFirstName, setValidFirstName] = useState(false);
	const [userFirstNameFocus, setUserFirstNameFocus] = useState(false);

	const [last_name, setUserLastName] = useState('');
	const [validLastName, setValidLastName] = useState(false);
	const [userLastNameFocus, setUserLastNameFocus] = useState(false);

	const [email, setUserEmail] = useState('');
	const [validEmail, setValidEmail] = useState(false);
	const [userEmailFocus, setUserEmailFocus] = useState(false);

	const [telephone, setUserTelephone] = useState('');
	const [validTelephone, setValidTelephone] = useState(false);
	const [userTelephoneFocus, setUserTelephoneFocus] = useState(false);

	const [profession, setUserProfession] = useState('');
	const [validProfession, setValidProfession] = useState(false);
	const [userProfessionFocus, setUserProfessionFocus] = useState(false);

	const [password, setPwd] = useState('');
	const [validPwd, setValidPwd] = useState(false);
	const [pwdFocus, setPwdFocus] = useState(false);

	const [matchPwd, setMatchPwd] = useState('');
	const [validMatch, setValidMatch] = useState(false);
	const [matchFocus, setMatchFocus] = useState(false);

	const [errMsg, setErrMsg] = useState('');
	const [success, setSuccess] = useState(false);

	useEffect(() => {
		userRef.current.focus();
	}, []);
	
	useEffect(() => {
		setValidName(USER_REGEX.test(username));
	}, [username]);

	useEffect(() => {
		setValidFirstName(USER_REGEX.test(first_name));
	}, [first_name]);

	useEffect(() => {
		setValidLastName(USER_REGEX.test(last_name));
	}, [last_name]);

	useEffect(() => {
		setValidEmail(EMAIL_REGEX.test(email));
	}, [email]);

	useEffect(() => {
		setValidTelephone(PHONE_REGEX.test(telephone));
	}, [telephone]);

	useEffect(() => {
		setValidProfession(PROFESSION_REGEX.test(profession));
	}, [profession]);

	useEffect(() => {
		setValidPwd(PWD_REGEX.test(password));
		setValidMatch(password === matchPwd);
	}, [password, matchPwd]);

	useEffect(() => {
		setErrMsg('');
	}, [username, first_name, first_name, email, telephone, profession, password, matchPwd]);

	const handleSubmit = async (e) => {
		e.preventDefault();
		// if button enabled with JS hack
		const v1 = USER_REGEX.test(username);
		const v2 = PWD_REGEX.test(password);
		if (!v1 || !v2) {
			setErrMsg('Invalid Entry');
			return;
		}
		try {
			let date_joined = "2018-04-12T00:00:00+0800";
			// console.log({ username, first_name, last_name, email, telephone, profession, password, date_joined });
			const response = await axios.post(
				REGISTER_URL,
				{
					"first_name": first_name,
					"last_name": last_name,
					"username": username,
					"email": email,
					"is_superuser": false,
					"is_staff": false,
					"is_active": false,
					"is_invitee": false,
					"telephone": telephone,
					"password": password,
					"date_joined": date_joined,
					"invited_by": null,
					"profession": parseInt(profession),
					"plan": 1
				},
				{
					headers: { 'Content-Type': 'application/json' },
					withCredentials: true,
					Vary: 'Accept'

				}
			);
			console.log(JSON.stringify(response?.data));
			setSuccess(true);
			//clear state and controlled inputs
			setUser('');
			setUserFirstName('');
			setUserLastName('');
			setUserEmail('');
			setUserTelephone('');
			setPwd('');
			setMatchPwd('');
		} catch (err) {
			if (!err?.response) {
				setErrMsg('No Server Response');
			} else if (err.response?.status === 409) {
				setErrMsg('Username Taken');
			} else {
				setErrMsg('Registration Failed');
			}
			errRef.current.focus();
		}
	};

	return (
		<>
			{success ? (
				<Login />
			) : (
				<section className='FormHolder'>
					<p
						ref={errRef}
						className={errMsg ? 'errmsg' : 'offscreen'}
						aria-live="assertive"
					>
						{errMsg}
					</p>
					<h1>Register</h1>
					<form onSubmit={handleSubmit}>
						<label htmlFor="username">
							Username:
							<FontAwesomeIcon
								icon={faCheck}
								className={validName ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validName || !username ? 'hide' : 'invalid'}
							/>
						</label>
						<input
							type="text"
							id="username"
							ref={userRef}
							autoComplete="off"
							onChange={(e) => setUser(e.target.value)}
							value={username}
							required
							aria-invalid={validName ? 'false' : 'true'}
							aria-describedby="uidnote"
							onFocus={() => setUserFocus(true)}
							onBlur={() => setUserFocus(false)}
						/>


						<label htmlFor="firstname">
							First name:
							<FontAwesomeIcon
								icon={faCheck}
								className={validFirstName ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validFirstName || !first_name ? 'hide' : 'invalid'}
							/>
						</label>
						<input
							type="text"
							id="first_name"
							ref={userRef}
							autoComplete="off"
							onChange={(e) => setUserFirstName(e.target.value)}
							value={first_name}
							required
							aria-invalid={validFirstName ? 'false' : 'true'}
							aria-describedby="uidnote"
							onFocus={() => setUserFirstNameFocus(true)}
							onBlur={() => setUserFirstNameFocus(false)}
						/>
						<p
							id="uidnote"
							className={
								userFirstNameFocus && first_name && !validFirstName ? 'instructions' : 'offscreen'
							}
						>
							<FontAwesomeIcon icon={faInfoCircle} />
							4 to 24 characters.
							<br />
							Must begin with a letter.
							<br />
							Numbers, underscores, hyphens are not allowed.
						</p>

						<label htmlFor="lastname">
							Last name:
							<FontAwesomeIcon
								icon={faCheck}
								className={validLastName ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validLastName || !last_name ? 'hide' : 'invalid'}
							/>
						</label>
						<input
							type="text"
							id="last_name"
							ref={userRef}
							autoComplete="off"
							onChange={(e) => setUserLastName(e.target.value)}
							value={last_name}
							required
							aria-invalid={validLastName ? 'false' : 'true'}
							aria-describedby="uidnote"
							onFocus={() => setUserLastNameFocus(true)}
							onBlur={() => setUserLastNameFocus(false)}
						/>
						<p
							id="uidnote"
							className={
								userLastNameFocus && last_name && !validLastName ? 'instructions' : 'offscreen'
							}
						>
							<FontAwesomeIcon icon={faInfoCircle} />
							4 to 24 characters.
							<br />
							Must begin with a letter.
							<br />
							Numbers, underscores, hyphens are not allowed.
						</p>

						<label htmlFor="email">
							E-mail:
							<FontAwesomeIcon
								icon={faCheck}
								className={validEmail ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validEmail || !email ? 'hide' : 'invalid'}
							/>
						</label>
						<input
							type="email"
							id="email"
							ref={userRef}
							autoComplete="off"
							onChange={(e) => setUserEmail(e.target.value)}
							value={email}
							required
							aria-invalid={validEmail ? 'false' : 'true'}
							aria-describedby="uidnote"
							onFocus={() => setUserEmailFocus(true)}
							onBlur={() => setUserEmailFocus(false)}
						/>
						<p
							id="uidnote"
							className={
								userEmailFocus && email && !validEmail ? 'instructions' : 'offscreen'
							}
						>
							<FontAwesomeIcon icon={faInfoCircle} />
							Please enter a valid email address. 
							<br />
							Exmple : youremail@exemple.com
						</p>
						
						<label htmlFor="telephone">
							Phone :
							<FontAwesomeIcon
								icon={faCheck}
								className={validTelephone ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validTelephone || !telephone ? 'hide' : 'invalid'}
							/>
						</label>
						<input
							type="text"
							id="telephone"
							ref={userRef}
							autoComplete="off"
							onChange={(e) => setUserTelephone(e.target.value)}
							value={telephone}
							required
							aria-invalid={validTelephone ? 'false' : 'true'}
							aria-describedby="uidnote"
							onFocus={() => setUserTelephoneFocus(true)}
							onBlur={() => setUserTelephoneFocus(false)}
						/>
						<p
							id="uidnote"
							className={
								userTelephoneFocus && telephone && !validTelephone ? 'instructions' : 'offscreen'
							}
						>
							<FontAwesomeIcon icon={faInfoCircle} />
							4 to 24 characters.
							<br />
							Letters, underscores, hyphens are not allowed.
						</p>

						<label htmlFor="profession">
							Profession :
							<FontAwesomeIcon
								icon={faCheck}
								className={validProfession ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validProfession || !profession ? 'hide' : 'invalid'}
							/>
						</label>
						<select value={profession} 
							onChange={(e) => setUserProfession(e.target.value)}
							onFocus={() => setUserProfessionFocus(true)}
							onBlur={() => setUserProfessionFocus(false)}
						>
							<option value=""></option>
							<option value="1">Accountant</option>
							<option value="2">Author</option>
							<option value="3">Architect</option>
							<option value="4">Designer</option>
							<option value="5">Travel agent</option>
							<option value="6">Secretary</option>
						</select>
						<p
							id="uidnote"
							className={
								userProfessionFocus && profession && !validProfession ? 'instructions' : 'offscreen'
							}
						>
							<FontAwesomeIcon icon={faInfoCircle} />
							4 to 24 characters.
							<br />
							Letters, underscores, hyphens are not allowed.
						</p>

						<label htmlFor="password">
							Password:
							<FontAwesomeIcon
								icon={faCheck}
								className={validPwd ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validPwd || !password ? 'hide' : 'invalid'}
							/>
						</label>
						<input
							type="password"
							id="password"
							onChange={(e) => setPwd(e.target.value)}
							value={password}
							required
							aria-invalid={validPwd ? 'false' : 'true'}
							aria-describedby="pwdnote"
							onFocus={() => setPwdFocus(true)}
							onBlur={() => setPwdFocus(false)}
						/>
						<p
							id="pwdnote"
							className={pwdFocus && !validPwd ? 'instructions' : 'offscreen'}
						>
							<FontAwesomeIcon icon={faInfoCircle} />
							8 to 24 characters.
							<br />
							Must include uppercase and lowercase letters, a number and a
							special character.
							<br />
							Allowed special characters:{' '}
							<span aria-label="exclamation mark">!</span>{' '}
							<span aria-label="at symbol">@</span>{' '}
							<span aria-label="hashtag">#</span>{' '}
							<span aria-label="dollar sign">$</span>{' '}
							<span aria-label="percent">%</span>
						</p>

						<label htmlFor="confirm_pwd">
							Confirm Password:
							<FontAwesomeIcon
								icon={faCheck}
								className={validMatch && matchPwd ? 'valid' : 'hide'}
							/>
							<FontAwesomeIcon
								icon={faTimes}
								className={validMatch || !matchPwd ? 'hide' : 'invalid'}
							/>
						</label>
						<input
							type="password"
							id="confirm_pwd"
							onChange={(e) => setMatchPwd(e.target.value)}
							value={matchPwd}
							required
							aria-invalid={validMatch ? 'false' : 'true'}
							aria-describedby="confirmnote"
							onFocus={() => setMatchFocus(true)}
							onBlur={() => setMatchFocus(false)}
						/>
						<p
							id="confirmnote"
							className={
								matchFocus && !validMatch ? 'instructions' : 'offscreen'
							}
						>
							<FontAwesomeIcon icon={faInfoCircle} />
							Must match the first password input field.
						</p>

						<button
							disabled={!validName || !validPwd || !validMatch ? true : false}
						>
							Sign Up
						</button>
					</form>
					<p>
						Already registered?
						<br />
						<span className="line">
							<a href="/login">Sign In</a>
						</span>
					</p>
				</section>
			)}
		</>
	);
};

export default Register;
